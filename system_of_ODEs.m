%filename system_of_ODEs.m
%% explaining the problem
%define a system of ordinary differential equations
% dCA/dt=-k1*CA-k2*CA
% dCB/dt=k1*CA-k3-k4*CB
% Equations describe the mass concentrations of 
%species A and B subject.
% unit of t is h(hour) and defines time
% The unit of CA and CB is mg L^-1(mg per liter)
% k1, k2, k3, and k4 are parametric values
% unites of k1, k2, k4 is h^-1  && unit of k3 is  mg* (L^-1)*(h^-1)
%% Function definition
function [dCA_t,dCB_t] = system_of_ODEs(varargin)
%the function can accept variable arguments using varargin
%dCA_t and dCB_t show the derivative with respect to time
%% case 1
  if nargin < 3 
      % nargin indicates how many input 
      %arguments a user has supplied.
      %if the user supplies an insufficient number of arguments
      error('you must supply the variables.');
  end
  %% case 2
  if nargin==3 
      % If only t and CA and CB are supplied,
      if varargin{1}>0
       % the value of time must be a positive number
  k1=0.15;
  k2=0.6;
  k3=0.1;
  k4=0.2;
  % default values for the rate constants k1; k2; k3 
  %and k4 should be specified as listed in Table
      dCA_t=-(k1+k2)*varargin{2};
      dCB_t= k1*varargin{2}-k3-k4*varargin{3};
      end
  end
 %% case 3
 if nargin>3
     % If t and C are not supplied,
     % their default values are t = 0 h
     % CA = 6.25 mg L^-1, and CB = 0 mg L^-1.
     dCA_t=-(varargin{1}+varargin{2})*6.25;
     dCB_t=varargin{1}*6.25-varargin{3};
  end
end
